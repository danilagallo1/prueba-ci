from django.test import TestCase


class HomepageTestCase(TestCase):

    def test_homepage_welcome(self):
        """
        Visiting the application homepage should display nice welcome message
        """
        response = self.client.get('http://127.0.0.1:8000/djangotest/')
        self.assertEqual(response.status_code, 200)
        self.assertContains(response,
                            "Hello, world. You're at the Django Test.")
